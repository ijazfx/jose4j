The jose.4.j library is an Apache 2 licensed open source implementation of JWS, JWE, JWA and JWK from the IETF JOSE Working Group. It is written in Java and relies solely on the JCA APIs for cryptography.

Please see https://bitbucket.org/b_c/jose4j/wiki/Home for more info.

